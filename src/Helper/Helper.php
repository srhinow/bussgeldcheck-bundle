<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\BussgeldCheckBundle\Helper;

use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\System;
use Psr\Log\LogLevel;

class Helper
{
    public static function createErrorLogEntry(string $message): void
    {
        self::createLogEntry($message, LogLevel::ERROR, ContaoContext::ERROR);
    }

    public static function createInfoLogEntry(string $message): void
    {
        self::createLogEntry($message, LogLevel::INFO, ContaoContext::GENERAL);
    }

    /**
     * @param string $type
     * @param string $context
     */
    public static function createLogEntry(string $message, $type = 'log', $context = 'GENERAL'): void
    {
        $logger = System::getContainer()->get('monolog.logger.contao');

        $logger->log(
            $type,
            $message,
            ['contao' => new ContaoContext(__METHOD__, $context)]
        );
    }
}
