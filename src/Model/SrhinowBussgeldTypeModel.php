<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\BussgeldCheckBundle\Model;

use Contao\Model;

class SrhinowBussgeldTypeModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_bussgeld_type';
}
