<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\BussgeldCheckBundle\Model;

use Contao\Model;

class SrhinowBussgeldCheckModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_bussgeld_check';
}
