<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\BussgeldCheckBundle\EventListener\Dca;

use Contao\Backend;
use Contao\DataContainer;
use Srhinow\BussgeldCheckBundle\Model\SrhinowBussgeldCheckModel;

class FormField extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    public function getBussgeldChecksForSelect(DataContainer $dc)
    {
        $arrReturn = [];

        $objBussgeldCheck = SrhinowBussgeldCheckModel::findAll();
        if (null === $objBussgeldCheck) {
            return $arrReturn;
        }

        while ($objBussgeldCheck->next()) {
            $arrReturn[$objBussgeldCheck->id] = $objBussgeldCheck->title;
        }

        return $arrReturn;
    }
}
