<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\BussgeldCheckBundle\EventListener\Dca;

use Contao\Backend;
use Contao\Controller;
use Contao\DataContainer;
use Contao\Image;
use Contao\StringUtil;
use Symfony\Component\Config\Definition\Exception\Exception;

class BussgeldType extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Autogenerate an bussgeld-type alias if it has not been set yet.
     *
     * @param mixed
     * @param object
     *
     * @return string
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate alias if there is none
        if (!\strlen($varValue)) {
            $autoAlias = true;
            $varValue = standardize($dc->activeRecord->title);
        }

        $objAlias = $this->Database->prepare('SELECT id FROM tl_bussgeld_type WHERE id=? OR alias=?')
            ->execute($dc->id, $varValue)
        ;

        // Check whether the page alias exists
        if ($objAlias->numRows > 1) {
            if (!$autoAlias) {
                throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-'.$dc->id;
        }

        return $varValue;
    }

    /**
     * Get all forms and return them as array.
     *
     * @return array
     */
    public function getForms()
    {
        if (!$this->User->isAdmin && !\is_array($this->User->forms)) {
            return [];
        }

        $arrForms = [];
        $objForms = $this->Database->execute('SELECT id, title FROM tl_form ORDER BY title');

        while ($objForms->next()) {
            if ($this->User->hasAccess($objForms->id, 'forms')) {
                $arrForms[$objForms->id] = $objForms->title.' (ID '.$objForms->id.')';
            }
        }

        return $arrForms;
    }

    /**
     * Return the edit form wizard.
     *
     * @return string
     */
    public function editForm(DataContainer $dc)
    {
        if ($dc->value < 1) {
            return '';
        }

        Controller::loadLanguageFile('tl_content');

//        dd($GLOBALS['TL_LANG']['tl_content']['editalias']);
        $title = sprintf($GLOBALS['TL_LANG']['tl_content']['editalias'], $dc->value);

        return ' <a href="contao/main.php?do=form&amp;table=tl_form_field&amp;id='
            .$dc->value.'&amp;popup=1&amp;nb=1&amp;rt='.REQUEST_TOKEN.'" title="'
            .StringUtil::specialchars($title).'" onclick="Backend.openModalIframe({\'title\':\''
            .StringUtil::specialchars(str_replace("'", "\\'", $title)).'\',\'url\':this.href});
            return false">'.Image::getHtml('alias.svg', $title).'</a>';
    }
}
