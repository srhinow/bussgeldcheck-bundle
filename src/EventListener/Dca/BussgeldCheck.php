<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\BussgeldCheckBundle\EventListener\Dca;

use Contao\Backend;
use Contao\DataContainer;
use Srhinow\BussgeldCheckBundle\Model\SrhinowBussgeldCheckModel;
use Symfony\Component\Config\Definition\Exception\Exception;

class BussgeldCheck extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Autogenerate an case-type alias if it has not been set yet.
     *
     * @param mixed
     * @param object
     *
     * @return string
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate alias if there is none
        if (!\strlen($varValue)) {
            $autoAlias = true;
            $varValue = standardize($dc->activeRecord->title);
        }

        $objAlias = $this->Database->prepare('SELECT id FROM tl_bussgeld_check WHERE id=? OR alias=?')
            ->execute($dc->id, $varValue)
        ;

        // Check whether the page alias exists
        if ($objAlias->numRows > 1) {
            if (!$autoAlias) {
                throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-'.$dc->id;
        }

        return $varValue;
    }

    public function getCheckOptions()
    {
        $arrOptions = [];
        $objCheck = SrhinowBussgeldCheckModel::findAll();
        if (null === $objCheck) {
            return $arrOptions;
        }

        while ($objCheck->next()) {
            $arrOptions[$objCheck->id] = $objCheck->title;
        }

        return $arrOptions;
    }
}
