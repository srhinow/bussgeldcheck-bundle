<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\BussgeldCheckBundle\EventListener\Hook;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Form;
use Contao\Widget;

/**
 * @Hook("loadFormField")
 */
class LoadFormFieldListener
{
    public function __invoke(Widget $widget, string $formId, array $formData, Form $form): Widget
    {
        if($widget->type === 'radio') {
            foreach($widget->options as $k => $option) {
                if(isset($option['default']) && $option['default'] == 1) {
                    $widget->value = $option['value'];
                }
            }
        }

        if (isset($_SESSION[$form->id.'_'.$widget->name])) {
            $widget->value = $_SESSION[$form->id.'_'.$widget->name];
        }


        return $widget;
    }
}
