function initHalfRadioButtons()
{
    let radio_items = $('.half_radio_buttons .radio_item');

    for (var i = 0; i < radio_items.length; i++) {
        // radio_items[i]
        if($(radio_items[i]).find('input[type=radio]').attr('checked') === 'checked') {
            $(radio_items[i]).find('label').addClass('active');
        }

        $(radio_items[i]).find('label').on('click',function($i){
            $(this).parents('.radio_item_group').find('label').removeClass('active');
            $(this).addClass('active');
        });
    }

}

function initChanngeCheck()
{
    let checkSelect = $('select[name=check]');
    let form = $(checkSelect).parents('form')[0];

    checkSelect.on('change', function (e) {
        e.preventDefault();
        let changeTo = $(form).find('input[name=change_to]')[0];
        if(!changeTo){
            $("<input>").attr({
                name: "change_to",
                type: "hidden",
                value: $(this).val()
            }).appendTo($(form));
        } else{
            $(changeTo).val($(this).val());
        }

        $(form).submit();
    });
}

jQuery(document).ready(function ($) {
    initChanngeCheck();
    initHalfRadioButtons();
});