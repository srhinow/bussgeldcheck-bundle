<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_DCA']['tl_form_field']['palettes']['bussgeldCheckSelect'] = '
{type_legend},type,name,label;
{options_legend},bussgeldCheckOptions;
{expert_legend:hide},class,accesskey,tabindex;
{template_legend:hide},customTpl;
{invisible_legend:hide},invisible';

$GLOBALS['TL_DCA']['tl_form_field']['fields']['bussgeldCheckOptions'] = [
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => ['srhinow_bussgeld_check.listeners.dca.form_field', 'getBussgeldChecksForSelect'],
    'eval' => ['chosen' => true, 'tl_class' => 'w50'],
    'sql' => 'smallint(5) unsigned NOT NULL default 0',
];
