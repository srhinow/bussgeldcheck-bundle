<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_bussgeld_check
 */
$GLOBALS['TL_DCA']['tl_bussgeld_check'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ctable' => ['tl_bussgeld_type'],
        'switchToEdit' => true,
        'enableVersioning' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'alias' => 'index',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'icon' => 'pagemounts.svg',
            'panelLayout' => 'filter;search',
        ],
        'label' => [
            'fields' => ['title'],
            'format' => '%s',
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_check']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.svg',
            ],
            'bussgeld_type' => [
                'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_check']['bussgeld_type'],
                'href' => 'table=tl_bussgeld_type',
                'icon' => $GLOBALS['BUSSGELDCHECK_PUBLIC_FOLDER'].'/icons/combo_boxes.png',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_check']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'cut' => [
                'label' => &$GLOBALS['TL_LANG']['tl_page']['cut'],
                'href' => 'act=paste&amp;mode=cut',
                'icon' => 'cut.svg',
                'attributes' => 'onclick="Backend.getScrollOffset()"',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_check']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\''.$GLOBALS['TL_LANG']['MSC']['deleteConfirm'].'\'))return false;
                Backend.getScrollOffset()"',
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        'default' => '{title_legend},title,alias',
    ],
    // Fields
    'fields' => [
        'id' => [
            'label' => ['ID'],
            'search' => true,
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'sorting' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_check']['title'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'decodeEntities' => true, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'alias' => [
            'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_check']['alias'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['doNotCopy' => true, 'maxlength' => 128, 'tl_class' => 'w50'],
            'save_callback' => [
                ['srhinow_bussgeld_check.listeners.dca.bussgeld_check', 'generateAlias'],
            ],
            'sql' => "varchar(128) COLLATE utf8_bin NOT NULL default ''",
        ],
    ],
];
