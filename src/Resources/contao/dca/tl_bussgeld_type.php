<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_bussgeld_type
 */
$GLOBALS['TL_DCA']['tl_bussgeld_type'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ptable' => 'tl_bussgeld_check',
        'switchToEdit' => true,
        'enableVersioning' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'alias' => 'pid,index',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 2,
            'fields' => ['sorting DESC'],
            'flag' => 1,
            'panelLayout' => 'filter;sort,search',
        ],
        'label' => [
            'fields' => ['title'],
            'format' => '%s',
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'href' => 'act=edit',
                'icon' => 'edit.svg',
            ],
            'copy' => [
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'cut' => [
                'href' => 'act=paste&amp;mode=cut',
                'icon' => 'cut.svg',
                'attributes' => 'onclick="Backend.getScrollOffset()"',
            ],
            'delete' => [
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\''.$GLOBALS['TL_LANG']['MSC']['deleteConfirm'].'\'))return false;
                Backend.getScrollOffset()"',
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__'                => array('do_subEvaluation'),
        'default' => '
        {title_legend},title,alias,form;
        {code_legend:hide},jsCode;
        {subEvaluation_legend},do_subEvaluation;
        {evaluation_legend},evaluation;
        {solution_legend},solutionText;
        {extend_legend},published',
    ],
    // Subpalettes
    'subpalettes' => [
        'do_subEvaluation'                   => 'subEvaluation'
    ],
    // Fields
    'fields' => [
        'id' => [
            'label' => ['ID'],
            'search' => true,
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'foreignKey' => 'tl_bussgeld_check.title',
            'search' => true,
            'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
            'eval' => [
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'sorting' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'decodeEntities' => true, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'alias' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['doNotCopy' => true, 'maxlength' => 128, 'tl_class' => 'w50'],
            'save_callback' => [
                ['srhinow_bussgeld_check.listeners.dca.bussgeld_check', 'generateAlias'],
            ],
            'sql' => "varchar(128) COLLATE utf8_bin NOT NULL default ''",
        ],
        'jsCode' => [
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'textarea',
            'eval'                    => array(
                'preserveTags'=>true,
                'decodeEntities'=>true,
                'class'=>'monospace',
                'rte'=>'ace|javascript',
                'tl_class'=>'clr',
                'nullIfEmpty'=>true
            ),
            'sql'                     => "text NULL"
        ],
        'form' => [
            'exclude' => true,
            'inputType' => 'select',
            'options_callback' => ['srhinow_bussgeld_check.listeners.dca.bussgeld_type', 'getForms'],
            'eval' => ['mandatory' => true, 'chosen' => true, 'tl_class' => 'clr wizard'],
            'wizard' => [
                ['srhinow_bussgeld_check.listeners.dca.bussgeld_type', 'editForm'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'evaluation' => [
            'exclude' => true,
            'inputType' => 'multiColumnWizard',
            'eval' => [
                'tl_class' => 'clr',
                'columnFields' => [
                    'condition' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_type']['condition'],
                        'exclude' => true,
                        'inputType' => 'text',
                        'eval' => ['style' => 'width:650px', 'decodeEntities' => true, 'allowHtml' => false],
                    ],
                    'points' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_type']['points'],
                        'exclude' => true,
                        'default' => '',
                        'inputType' => 'text',
                        'eval' => ['style' => 'width:30px'],
                    ],
                    'driving_ban' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_type']['driving_ban'],
                        'exclude' => true,
                        'default' => '',
                        'inputType' => 'text',
                        'eval' => ['style' => 'width:100px'],
                    ],
                    'fine' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_type']['fine'],
                        'exclude' => true,
                        'inputType' => 'text',
                        'eval' => ['style' => 'width:150px'],
                    ],
                ],
            ],
            'sql' => 'blob NULL',
        ],
        'do_subEvaluation' => [
            'label'                   => &$GLOBALS['TL_LANG']['tl_spree_case']['do_subEvaluation'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('submitOnChange'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ],
        'subEvaluation' => [
            'exclude' => true,
            'inputType' => 'multiColumnWizard',
            'eval' => [
                'tl_class' => 'clr',
                'columnFields' => [
                    'condition' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_type']['condition'],
                        'exclude' => true,
                        'inputType' => 'text',
                        'eval' => ['style' => 'width:450px', 'decodeEntities' => true, 'allowHtml' => false],
                    ],
                    'tag' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_bussgeld_type']['tag'],
                        'exclude' => true,
                        'default' => '',
                        'inputType' => 'text',
                        'eval' => ['style' => 'width:150px'],
                    ],
                ],
            ],
            'sql' => 'blob NULL',
        ],
        'solutionText' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['mandatory' => true, 'rte' => 'tinyMCE', 'helpwizard' => true],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'published' => [
            'exclude' => true,
            'filter' => true,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
    ],
];
