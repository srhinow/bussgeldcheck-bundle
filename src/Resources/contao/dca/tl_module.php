<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_DCA']['tl_module']['palettes']['check_forms'] = '{title_legend},name,type;{defaultForm_legend},form';
