<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_bussgeld_check']['title'] = ['Name des Bussgeld-Checkes'];
$GLOBALS['TL_LANG']['tl_bussgeld_check']['alias'] = ['Alias'];

$GLOBALS['TL_LANG']['tl_bussgeld_check']['form'] = [
    'Formular',
    'weisen Sie hier das Formular aus de Formulargenerator zu was angezeigt wird wenn dieser Type ausgewählt wurde',
];
$GLOBALS['TL_LANG']['tl_bussgeld_check']['evaluation'] = ['Auswertung'];
$GLOBALS['TL_LANG']['tl_bussgeld_check']['condition'] = [
    'Bedingung',
    'Das Ergebnis muss ein bool-Wert (true/false) sein. Platzhalter für die Feld-Werte sind dann ##feld_name##.',
];
$GLOBALS['TL_LANG']['tl_bussgeld_check']['driving_ban'] = ['Fahrverbot'];
$GLOBALS['TL_LANG']['tl_bussgeld_check']['points'] = ['Punkte'];
$GLOBALS['TL_LANG']['tl_bussgeld_check']['fine'] = ['Strafe'];
$GLOBALS['TL_LANG']['tl_bussgeld_check']['solutionText'] = [
    'Ergebnis-Text',
    'Platzhalter sind dann auch wieder %points%,%driving_ban%,%fine% für die Ergebnisse und 
    ##formname__name## oder ##formname__value## für die Formularfelder.',
];
$GLOBALS['TL_LANG']['tl_bussgeld_check']['published'] = ['veröffentlicht'];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bussgeld_check']['new'][0] = 'Neuer Bussgeld-Check';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['new'][1] = 'Einen neuen Bussgeld-Check anlegen.';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['bussgeld_type'][0] = 'Check-Typen verwalten';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['bussgeld_type'][1] = 'Hier werden die Check-Typen verwalten';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['edit'][0] = 'Bussgeld-Check bearbeiten';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['edit'][1] = 'Bussgeld-Check ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['copy'][0] = 'Bussgeld-Check duplizieren';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['copy'][1] = 'Bussgeld-Check ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['delete'][0] = 'Bussgeld-Check löschen';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['delete'][1] = 'Bussgeld-Check ID %s löschen.';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['show'][0] = 'Bussgeld-Check-Details anzeigen';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['show'][1] = 'Details für den Bussgeld-Check ID %s anzeigen.';

/*
 * legends
 */
$GLOBALS['TL_LANG']['tl_bussgeld_check']['title_legend'] = 'Titel-Einstellungen';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['main_legend'] = 'Haupt-Einstellungen';
$GLOBALS['TL_LANG']['tl_bussgeld_check']['extend_legend'] = 'weitere Einstellungen';
