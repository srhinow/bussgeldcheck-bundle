<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Back end modules.
 */
$GLOBALS['TL_LANG']['MOD']['bussgeld_check'] = ['Bussgeld-Check', 'Die Verwaltung von Bussgeld-Checks'];

/*
 * Front end modules.
 */
//$GLOBALS['TL_LANG']['FE_MOD']['casemanager'] = ['Fälle', 'Die Fall-Verwaltung'];
//$GLOBALS['TL_LANG']['FE_MOD']['case_form_generic'] = [
//    'Fall Generik-Formular',
//    'Ausgabe und Verarbeitung der Formulardaten als JSON-Abspeicherung.'
//];
