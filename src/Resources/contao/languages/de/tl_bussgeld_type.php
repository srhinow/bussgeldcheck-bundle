<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_bussgeld_type']['title'] = ['Name des Check-Types'];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['alias'] = ['Alias'];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['jsCode'] = ['JS-Code'];

$GLOBALS['TL_LANG']['tl_bussgeld_type']['form'] = [
    'Formular',
    'weisen Sie hier das Formular aus de Formulargenerator zu was angezeigt wird wenn dieser Type ausgewählt wurde',
];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['do_subEvaluation'] = ['zusätzliche Vorberechnungen anlegen'];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['subEvaluation'] = ['Vorberechnungen','hier können auf Grundlage der Feld-Werte zusätzliche Platzhalteangelegt werden. z.B. intval(##speed##)/2 | half_speed'];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['evaluation'] = ['Auswertung'];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['condition'] = [
    'Bedingung',
    'Das Ergebnis muss ein bool-Wert (true/false) sein. Platzhalter für die Feld-Werte sind dann ##feld_name##.',
];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['driving_ban'] = ['Fahrverbot'];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['points'] = ['Punkte'];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['fine'] = ['Strafe'];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['solutionText'] = [
    'Ergebnis-Text',
    'Platzhalter sind dann auch wieder %points%,%driving_ban%,%fine% für die Ergebnisse und 
    ##formname__name## oder ##formname__value## für die Formularfelder.',
];
$GLOBALS['TL_LANG']['tl_bussgeld_type']['published'] = ['veröffentlicht'];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bussgeld_type']['new'][0] = 'Neuer Check-Typ';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['new'][1] = 'Einen neuen Check-Typ anlegen.';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['edit'][0] = 'Check-Typ bearbeiten';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['edit'][1] = 'Check-Typ ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['copy'][0] = 'Check-Typ duplizieren';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['copy'][1] = 'Check-Typ ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['delete'][0] = 'Check-Typ löschen';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['delete'][1] = 'Check-Typ ID %s löschen.';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['show'][0] = 'Check-Typ-Details anzeigen';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['show'][1] = 'Details für den Check-Typ ID %s anzeigen.';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['case_check_configuration'] = [
    'Check-Typ-Konfigurationen',
    'Hier werden die Check-Typ-Konfigurationen verwalten',
];

/*
 * legends
 */
$GLOBALS['TL_LANG']['tl_bussgeld_type']['title_legend'] = 'Titel-Einstellungen';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['code_legend'] = 'Javascripte für die Formular-Kontrolle';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['subEvaluation_legend'] = 'Vorberechnung';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['evaluation_legend'] = 'Auswertungen';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['solution_legend'] = 'Ergebnis-Einstellungen';
$GLOBALS['TL_LANG']['tl_bussgeld_type']['extend_legend'] = 'weitere Einstellungen';
