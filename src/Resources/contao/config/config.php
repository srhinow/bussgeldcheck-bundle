<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['BUSSGELDCHECK_PUBLIC_FOLDER'] = 'bundles/srhinowbussgeldcheck';

/*
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------.
 */
$GLOBALS['BE_MOD']['content']['bussgeld_check'] = [
    'tables' => ['tl_bussgeld_check', 'tl_bussgeld_type'],
];

/*
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
$GLOBALS['FE_MOD']['bussgeld_check'] = [
    'check_forms' => 'Srhinow\BussgeldCheckBundle\ModuleBussgeldCheck',
];

/*
 * -------------------------------------------------------------------------
 * Form-Fields
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_FFL']['bussgeldCheckSelect'] = \Srhinow\BussgeldCheckBundle\Widget\BussgeldCheckSelect::class;

/*
 * -------------------------------------------------------------------------
 * Models
 * -------------------------------------------------------------------------.
 */
$GLOBALS['TL_MODELS']['tl_bussgeld_check'] = \Srhinow\BussgeldCheckBundle\Model\SrhinowBussgeldCheckModel::class;
$GLOBALS['TL_MODELS']['tl_bussgeld_type'] = \Srhinow\BussgeldCheckBundle\Model\SrhinowBussgeldTypeModel::class;
