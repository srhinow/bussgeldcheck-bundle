<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\BussgeldCheckBundle;

use Contao\FormFieldModel;
use Contao\Input;
use Contao\Module;
use Contao\StringUtil;
use Srhinow\BussgeldCheckBundle\Helper\Helper;
use Srhinow\BussgeldCheckBundle\Model\SrhinowBussgeldTypeModel;

/**
 * Provides methodes to handle BuccgeldCheck-Type-Forms.
 */
class ModuleBussgeldCheck extends Module
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'mod_bussgeld_check';

    /**
     * SrhinowBussgeldTypeModel.
     *
     * @var object|null
     */
    protected $objCheckType;

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### BUSSGELD-CHECK ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * da der PrepareFormData-Hook nicht greift, die Post--Daten hier in der Session speichern.
     *
     * @param $submittedData
     * @param $formId
     */
    public function refreshFieldsInSession($submittedData, $formId): void
    {
        foreach ($submittedData as $k => $value) {
            $_SESSION[$formId.'_'.$k] = $value;
        }
        $_SESSION['check_formId'] = $formId;
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        $formId = ($_SESSION['check_formId']) ?: $this->form;

        if (isset($_SESSION[$formId.'_check'])) {
            $checkId = $_SESSION[$formId.'_check'];
        }

        if (Input::post('check')) {
            $checkId = Input::post('check');
        }

        if (isset($checkId)) {
            $this->objCheckType = SrhinowBussgeldTypeModel::findByIdOrAlias($checkId);
            if (null !== $this->objCheckType) {
                $formId = $this->objCheckType->form;
            }
        }
        $strFormId = 'auto_form_'.$formId;

        $this->Template->formId = $formId;
        $this->Template->send = false;

        if (null !== Input::post('change_to')) {
            $_SESSION['check_formId'] = $this->objCheckType->form;
            $_SESSION[$this->objCheckType->form.'_check'] = Input::post('change_to');
        }

        if (Input::post('FORM_SUBMIT') === $strFormId && !Input::post('change_to')) {
            $this->Template->send = true;
            $this->setSolutionOutput();
        }

        if (isset($this->objCheckType->jsCode)) {
            $this->Template->jsCode = $this->objCheckType->jsCode;
        }

        $GLOBALS['TL_JAVASCRIPT']['bgc'] = $GLOBALS['BUSSGELDCHECK_PUBLIC_FOLDER'].'/js/bussgeldcheck_bundle.js';
    }

    protected function setSolutionOutput(): void
    {
        if (null === $this->objCheckType) {
            return;
        }

        $objFormFields = FormFieldModel::findBy(['pid=?'], [$this->objCheckType->form]);
        if (null === $objFormFields) {
            return;
        }

        while ($objFormFields->next()) {
            if ('' === $objFormFields->name) {
                continue;
            }
            $value = Input::post($objFormFields->name);
            $arrFieldValues[$objFormFields->name] = $value;
            $arrFieldLabels[$objFormFields->name.'_label'] = $this->getValueByFieldType($value, $objFormFields->current());
        }

        //Session-Daten aktualisieren
        $this->refreshFieldsInSession($arrFieldValues, $this->objCheckType->form);

        $this->Template->solutionText = '';

        if($this->objCheckType->do_subEvaluation) {
            $arrFieldValues = $this->setSubEvaluations($arrFieldValues);
        }

        //hole wenn vorhanden die passende Bedingung (MCW- Reihe)
        $evaluation = $this->getRightCondition($arrFieldValues);

        // Wenn eine Bedingung gefunden wurde ersetzte die Platzhalter im Text und setze es für das Template
        if ($evaluation) {
            //auch nochmal die möglichen Ersetzungen/Ergänzungen von den Strafen für den solutionText
            if($this->objCheckType->do_subEvaluation) {
                $arrSubEvaluation = $this->setSubEvaluations($evaluation);
                $arrFieldLabels = array_merge($arrFieldLabels,$arrSubEvaluation);
            }

            $solutionText = StringUtil::parseSimpleTokens($this->objCheckType->solutionText, $evaluation);
            $solutionText = StringUtil::parseSimpleTokens($solutionText, $arrFieldLabels);
            $solutionText = StringUtil::parseSimpleTokens($solutionText, $arrFieldValues);
            $this->Template->solutionText = $solutionText;
        }
    }

    /**
     * @param array $arrFieldValues
     * @return array
     * @throws \Exception
     */
    protected function setSubEvaluations($arrFieldValues)
    {
        $arrSubEvals = unserialize($this->objCheckType->subEvaluation);
        if(!is_array($arrSubEvals) || count($arrSubEvals) < 1) {
            return $arrFieldValues;
        }

        foreach($arrSubEvals as $subeval) {
            $condition  = StringUtil::parseSimpleTokens($subeval['condition'],$arrFieldValues);

            //wenn Platzhalter nicht ersetzt werden konnten den Eintrag überspringen
            if(stristr($condition,'##')) {
                continue;
            }

            try {
                $solution = false;
                eval('$solution = '.html_entity_decode($condition).';');

                if ($solution) {
                    $arrFieldValues[$subeval['tag']] = $solution;
                }
            } catch (\Throwable $t) {
//                dump($arrFieldValues);
//                dump($t->getMessage().' in '.$condition); die();
                Helper::createErrorLogEntry($t->getMessage());
                return $arrFieldValues;
            }

        }

        return $arrFieldValues;
    }

    /**
     * läuft alle evaluation-Einträge durch und gibt wenn einer zutrifft diesen Eintrag zurück.
     *
     * @param $arrFieldValues
     *
     * @throws \Exception
     *
     * @return bool|mixed
     */
    protected function getRightCondition($arrFieldValues)
    {
        $arrEvaluation = unserialize($this->objCheckType->evaluation);
        foreach ($arrEvaluation as $item) {
            $condition = htmlspecialchars_decode($item['condition']);
            $condition = StringUtil::parseSimpleTokens($condition, $arrFieldValues);

            try {
                $bool = false;
                eval('$bool = '.html_entity_decode($condition).';');

                if ($bool) {
                    return $item;
                }
            } catch (\Throwable $t) {
//                dump($condition);
//                dump($t->getMessage()); die();
                Helper::createErrorLogEntry($t->getMessage());
                return false;
            }
        }

        return false;
    }

    /**
     * holt je nach Feldtyp (tl_form_field) den entsprechenden Gegenwert aus den Einstellungen.
     *
     * @param $value
     * @param $objFormField
     */
    protected function getValueByFieldType($value, $objFormField)
    {
        switch ($objFormField->type) {
            case 'select':
            case 'radio':
                $arrOptions = unserialize($objFormField->options);
                $newValue = $this->getRightLabelFromOptions($value, $arrOptions);
                break;
            default:
                $newValue = $value;
        }

        return $newValue;
    }

    /**
     * holt aus den Feld-Einstellungen (form-generator) das zum gesendeten value passende Label.
     *
     * @param $value
     * @param $arrOptions
     */
    protected function getRightLabelFromOptions($value, $arrOptions)
    {
        if (!\is_array($arrOptions) || \count($arrOptions) < 1) {
            return $value;
        }

        $returnLabel = $value;
        foreach ($arrOptions as $k => $item) {
            if ($value !== $item['value']) {
                continue;
            }
            $returnLabel = $item['label'];
        }

        return $returnLabel;
    }
}
