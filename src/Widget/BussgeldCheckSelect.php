<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension bussgeldcheck-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\BussgeldCheckBundle\Widget;

use Contao\SelectMenu;
use Srhinow\BussgeldCheckBundle\Model\SrhinowBussgeldTypeModel;

class BussgeldCheckSelect extends SelectMenu
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'form_bussgeldCheckSelect';

    /**
     * Add specific attributes.
     *
     * @param string $strKey
     */
    public function __set($strKey, $varValue): void
    {
        switch ($strKey) {
            case 'bussgeldCheckOptions':
                $this->arrOptions = $this->getBussgeldTypesFromCheck($varValue);
                break;

            default:
                parent::__set($strKey, $varValue);
                break;
        }
    }

    protected function getBussgeldTypesFromCheck($check = null): array
    {
        $arrReturn = [];
        if ((int) $check < 1) {
            return $arrReturn;
        }

        $objCheckTypes = SrhinowBussgeldTypeModel::findBy(['pid=?', 'published=?'], [$check, '1']);
        if (null === $objCheckTypes) {
            return $arrReturn;
        }

        while ($objCheckTypes->next()) {
            $arrReturn[] = [
                'label' => $objCheckTypes->title,
                'value' => $objCheckTypes->id,
            ];
        }

        return $arrReturn;
    }
}
